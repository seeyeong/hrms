<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

	/**
	 * Get all of the families for the Employee
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function families()
	{
		return $this->hasMany(EmployeeFamily::class);
	}

	/**
	 * Get all of the education for the Employee
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function education()
	{
		return $this->hasMany(EmployeeEducation::class);
	}
}
